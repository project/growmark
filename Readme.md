# Growmark theme

Growmark, a Drupal 10 supported theme, developed by Dotsquares  and is fully responsive.

This theme is very suitable for all types of websites; from corporate site to blog site.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/).


## Installation

Install as you would normally install a contributed Drupal Theme. For further
information, see
[Installing Drupal Themes](https://www.drupal.org/docs/extending-drupal/installing-themes).

## Configuration

1. Enable the thems at Administration > Appearance.
2. Go to theme setting page set all the fields listed on the page.
3. Enable webform,for create webform.
4. When creating a webform field on a content type or custom entity type, choose
   "webform" from the drop-down menu.

## Maintainers

- Mansi Agarwal - [mansi-agarwal](https://www.drupal.org/u/mansi-agarwal)
- Janvi Dasani - [janvi-dasani](https://www.drupal.org/u/janvi-dasani)