<?php

/**
 * @file
 * growmark theme settings.
 */

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function growmark_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {

 // Footer -> Copyright.
  $form['footer']['copyright'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Website Copyright Text'),
  ];

  $form['footer']['copyright']['copyright_text'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show website copyright text in footer.'),
    '#default_value' => theme_get_setting('copyright_text', 'growmark'),
    '#description'   => t("Check this option to show website copyright text in footer. Uncheck to hide."),
  ];

   // Settings under social tab.
  // Show or hide all icons.
  $form['social']['all_icons'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Show Social Icons'),
  ];

  $form['social']['all_icons']['all_icons_show'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show social icons in header'),
    '#default_value' => theme_get_setting('all_icons_show', 'growmark'),
    '#description'   => t("Check this option to show social icons in header. Uncheck to hide."),
  ];
}
